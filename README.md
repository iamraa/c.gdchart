GDChart v0.11.5dev

Source: [Author website](http://users.fred.net/brv//chart/)

### Before compile at Ubuntu ###

Install GD library:

    $ sudo apt-get install libgd2-noxpm-dev

Find where installed `gdlib.so`:

    $ which gdlib-config # where is installed library config
    $ gdlib-config --libdir # where is libgd.so

Change `GD_LD` path in `Makefile` to your path. For example:

    GD_LD=/usr/lib/x86_64-linux-gnu

### How to compile ###

Go to the path with GDChart sources and run:

    $ make

After that library was compiled to static library `libgdc.a`
with executable samples.

### How to use ###

Samples you can find in `*_samp#.c` files:

* `gdc_pie_samp` - generate `pie.png` with 3D-pie. Run: `./gdc_pie_samp1`
* `gdc_samp1` - generate gif with 3D-bar chart. Run: `./gdc_samp1 > gdc_samp1.gif`
* `gdc_samp2` - generate `g2.png` with HLC and Volume area & annotation. Run: `./gdc_samp2`
* `ft_samp` - generate png with 3D-bar chart and font using samples. Run: `./ft_samp > ft_samp.png`